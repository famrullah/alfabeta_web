<?php include 'navbar.php'; ?>
    <div class="banner-contact">
        <div class="white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">                       
                    </div>
                    <div class="col-sm-6">
                        <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Et cum ad alias sed aliquam, repudiandae neque facere nihil dolorem perferendis. 
                            Consectetur odio quas eveniet doloribus, hic magnam animi voluptates fugit.
                        </p>
                    </div>
                </div>
            </div>            
        </div>
    </div>    
    <div class="address f-cairo">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bord">
                    <h2 class="address-title">alfabeta office address</h2>
                    <p class="f-200">Jl.Talavera Office Suite lt.18 
                        jl tb.Simatupang kav.22-26
                        <br>
                        Cilandak barat, Jakarta Selatan
                    </p>
                    <br><br>
                    <p class="f-200">
                        <span class="bold">Phone:</span> 
                        <br><br>
                        021 - 71790313
                    </p>
                    <div class="form-social">
                        <div class="menu">
                            <ul>
                                <li class="facebook"><a  href=""><i class="fab fa-facebook-f"></i></a></li>
                                <li class="google"><a href=""><i class="fab fa-google-plus-g"></i></a></li>
                                <li class="twitter"><a href=""><i class="fab fa-twitter"></i></a></li>
                                <li class="linkedin"> <a href=""><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="email f-cairo"><span> <i class="far fa-envelope"></i></span>&nbsp;&nbsp;&nbsp;contact@alfabeta.co.id</div>
                    <div class="phone"> <span><i class="fas fa-phone"></i></span>&nbsp;&nbsp;&nbsp;(021) 727 1267</div>
                </div>
                <div class="col-sm-6 form">
                    <h2>Lets Gets in Touch !</h2>
                    <form>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="form-control" placeholder="First name">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                        </div>
                        <div class="form-row text-area">
                            <textarea name="" id="" cols="90" rows="10"></textarea>                           
                        </div>
                        <div class="form-row">
                           <button>Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.092599247323!2d106.8171451146543!3d-6.251528695474925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3d647612ef5%3A0x7a2d68aab29a847f!2sPT.+Alfabeta+Solusi+Nusantara+(Development+HQ)!5e0!3m2!1sen!2sid!4v1531912579550" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
<?php include 'footer.php'; ?>