<?php include 'navbar.php'; ?>    
    <div class="banner-product banner-plate">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="f-cairo">OCR (Optical Character Recognition)</h1>
                </div>
                <div class="col-sm-6">
                    <p class="f-cairo f-w-200 f-14">
                    Aplikasi yang berfungsi untuk scan image dan dijadikan text, aplikasi ini dapat menjadi 
                    support tambahan untuk scanner
                    </p>
                </div>
            </div>
        </div>            
    </div>    
    <div class="product-page">
        <div class="product-page__img">
            <img src="./images/img-area of interest-4.png" alt="">
        </div>
        <div class="product-page__desc">
            <p class="f-cairo">Aplikasi yang berfungsi untuk scan image dan dijadikan text, aplikasi ini dapat menjadi 
                    support tambahan untuk scanner
            </p>
        </div>
    </div>
    
    <!-- <div class="product__bg">
        <div class="desc">
            <p class="white">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.\
                In omnis vitae consequatur maxime. Assumenda doloremque similique distinctio modi ad nemo corporis vitae animi vel, quis pariatur sunt sequi, numquam quibusdam!
            </p>
        </div>
    </div> -->

    <!-- embed video -->
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h2 class="embed-video"><i>OCR (Optical Character Recognition)</i></h2>
            </div>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="videos/ocr_video.mp4"></iframe>
            </div>
        </div>
    </div>           

    <div class="our-partners">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h2 class="f-cairo">Our Partner</h2>
                    </div>                        
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">   
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">   
                </div>
            </div>
        </div>
    </div>
    
<?php include 'footer.php'; ?>