<?php include 'header.php'; ?>
    <div class="banner">
        <div class="banner-content">
            <div class="left-banner">
                <h1>Striving Over New Technology</h1>
                <p>Alfabeta adalah penyedia solusi berupa produk dan layanan end-to-end di berbagai industri</p>
            </div> 
            <div class="swiper-container right-banner">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="images/img-org.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="images/kerumunan.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="images/mobil.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="images/cctv.png" alt="">
                    </div>
                    <div class="swiper-slide">
                        <img src="images/img-org.png" alt="">
                    </div>
                </div>
            </div>
                                   
        </div>          
    </div>            
       <!-- Product page -->
    <div class="product">
        <!-- <div class="product__solution">
            <div class="top-title">
                <h2 class="f-cairo">Product By Solutions</h2>
                <p class="f-cairo">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div class="product__solution-list">
                <div class="centered">                    
                    <div class="content" style="margin-top:10px;">
                        <div class="icon">
                            <a href="smart-parking.php"><img src="images/ic-parkir.png" alt=""></a>
                        </div>
                        <div class="text">
                            <h5 class="title"><a href="smart-parking.php">Smart Parking</ a></h5>
                        </div>
                        <div class="text f-cairo">
                        <p class="f-14 f-w-200"><a href="product.php"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum debitis voluptas dolorem aspernatur.</a></p>
                            <a href="product.php">Learn more</a>
                            <p class="float-r"><i class="fas fa-arrow-right"></i></p>
                        </div>
                    </div>
                    <div class="content">
                        <div class="icon">
                            <a href="smart-absence.php"><img src="images/ic-mobil.png" alt=""></a>
                        </div>
                        <div class="text">
                            <h5 class="title"> <a href="smart-absence.php">Smart Absence Management</a></h5>
                        </div>
                        <div class="text f-cairo">
                            <p class="f-14 f-w-200"><a href="smart-absence.php"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum debitis voluptas dolorem aspernatur.</a></p>
                            <a href="smart-absence.php">Learn more</a>
                            <p class="float-r"><i class="fas fa-arrow-right"></i></p>
                        </div>
                    </div>
                    <div class="content">
                        <div class="icon">
                            <a href="perimeter.php"><img src="images/ic-rumah.png" alt=""> </a>
                        </div>
                        <div class="text">
                            <h5 class="title"> <a href="perimeter.php">Smart Perimeter Security</a></h5>
                        </div>
                        <div class="text f-cairo">
                            <p class="f-14 f-w-200"><a href="perimeter.php"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum debitis voluptas dolorem aspernatur.</a></p>
                            <a href="perimeter.php">Learn more</a>
                           <p class="float-r"><i class="fas fa-arrow-right"></i></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product__solution-list ">
                <div class="centered">
                        <div class="content">
                            <div class="icon">
                                <a href="smart-parking.php"><img src="images/ic-cctv.png" alt=""></a> 
                            </div>
                            <div class="text">
                                <h5 class="title"><a href="smart-disaster.php">Smart Disaster and Anomaly Detection</a></h5>
                            </div>
                            <div class="text f-cairo">
                                <p class="f-14 f-w-200"><a href="smart-disaster.php"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum debitis voluptas dolorem aspernatur.</a></p>
                                <a href="smart-disaster.php">Learn more</a>
                                <p class="float-r"><i class="fas fa-arrow-right"></i></p>
                            </div>
                        </div>                    
                        <div class="content">
                            <div class="icon">
                                <a href="smart-parking.php"><img src="images/ic-people.png" alt=""></a>
                            </div>
                            <div class="text">
                                <h5 class="title"><a href="smart-disaster.php">Smart Disaster and Anomaly Detection</a></h5>
                            </div>
                            <div class="text f-cairo">
                                <p class="f-14 f-w-200"><a href="smart-disaster.php"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum debitis voluptas dolorem aspernatur.</a></p>
                                <a href="smart-disaster.php">Learn more</a>
                                <p class="float-r"><i class="fas fa-arrow-right"></i></p>
                            </div>
                        </div>                    
                </div>
            </div>
        </div> -->
        <!-- <div class="background"></div>
        <div class="promotion">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <p>
                            We Cant Wait To What You Build Free Options and free Advice.
                            <br>
                            Everything you need to get started . We Wait to See What You
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="promotion-btn">
                            <a href="">3 months</a>
                            <a href="">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="product__modular">
            <div class="container">
                <div class="product-modular__title">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="f-cairo"> Product </h2>
                        </div>
                        <!-- <div class="col-sm-8 p-20">
                            <p class="f-cairo">
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod quis sed fug
                                doloremque aliquam dolorem error quasi porro ab. 
                                Corrupti possimus perspiciatis quam cum dolore iste nihil minus maiores quis.
                            </p>                        
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="plate.php"><img class="license-plate" src="images/licenseplate.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="plate.php" class="white"> License Plate Recognition</a></h5>
                                    </div>
                                   <!--  <a href="" class="white f-cairo">
                                         Mendeteksi / mengenali Plat Registrasi kendaraan dengan teknologi yang menggunakan pengenalan karakter optic pada data visual/ gambar
                                    </a> -->
                                </div>
                            </div>
                        </div>                                
                    </div>    

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="flood.php"><img class="traffic-sign.png" src="images/flood.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                    <h5><a href="flood.php" class="white"> Flood <br>Detection</a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>                                
                    </div>    

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="trash.php"><img class="license-plate" src="images/trash.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="trash.php" class="white"> Trash <br>Detection</a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>                                
                    </div>    

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="fire-detection.php"><img class="license-plate" src="images/fire.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="fire-detection.php" class="white"> Fire <br>Detection</a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>                                
                    </div>  

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="face-detection.php"><img class="license-plate" src="images/face.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="face-detection.php" class="white"> Face <br>Recognition</a></h5>
                                    </div>                                                                         
                                </div>
                            </div>
                        </div>                                
                    </div>     

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="plate.php"><img class="license-plate" src="images/human.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="human-detection.php" class="white"> Human <br> Detection</a></h5>
                                    </div>     
                                </div>
                            </div>
                        </div>                                
                    </div>   

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="plate.php"><img class="license-plate" src="images/intruder.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                    <h5><a href="intruder.php" class="white"> Intruder <br> Detection</a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>                                
                    </div>   

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="vehicle.php"><img class="license-plate" src="images/52.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="vehicle.php " class="white"> Vehicle Detection & Counting</a></h5>
                                    </div>      
                                </div>
                            </div>
                        </div>                                
                    </div>                             
                    
                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="ocr.php"><img class="license-plate" src="images/OCR.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="ocr.php " class="white"> OCR (Optical Character Recognition)</a></h5>
                                    </div>     
                                </div>
                            </div>
                        </div>                                
                    </div>  

                    <div class="product-modular__list col-sm-4">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <a href="crowd.php"><img class="license-plate" src="images/crowd.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="product__solution-text f-cairo">
                                    <div class="title">
                                        <h5><a href="crowd.php " class="white"> Crowd <br>Detection</a></h5>
                                    </div>     
                                </div>
                            </div>
                        </div>                                
                    </div>                             
                </div>                
            </div>
        </div>
    </div>

    <div class="area-of__interest">
        <div class="container area-f__content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h2 class="f-cairo">Area of Interest</h2>
                        <p class="f-cairo f-14">Lorem ipsum dolor sit amet consectetur adipisicing elit.sed officiis pariatur minima minus eius!</p>
                    </div>
                    <div class="row">                    
                        <div class="col-sm-4">
                            <div class="list-of__area">
                                <div class="img__">
                                    <a href="plate.php"><img src="./images/produk-disaster.jpg" alt=""></a> 
                                </div>                                
                                <div class="area__text"> 
                                    <div class="header">
                                        <p class="f-cairo"><a href="smart-disaster.php" class="white"> Smart Disaster Prevention Solutions</a></p>
                                    </div>  
                                    <div class="desc bg-disaster">
                                        <p class="f-cairo"><a href="smart-disaster.php" class="white"> Lorem ipsum dolor sit amet consectetur adipisicing elit. sed officiis pariatur minima minus eius!</a></p>
                                    </div>                          
                                </div>                                                                
                            </div>                                                       
                        </div>                   
                        <div class="col-sm-4">
                            <div class="list-of__area">
                                <div class="img__">
                                    <a href="plate.php"><img src="./images/produk-parking.jpg" alt=""></a>
                                </div>                                
                                <div class="area__text"> 
                                    <div class="header">
                                    <p class="f-cairo"><a href="product.php" class="white"> Smart Parking Solution</a></p>
                                    </div>  
                                    <div class="desc bg-parking">
                                        <p class="f-cairo"><a href="product.php" class="white"> Lorem ipsum dolor sit amet consectetur adipisicing elit. sed officiis pariatur minima minus eius!</a></p>
                                    </div>                          
                                </div>                                                                
                            </div>                                                       
                        </div>                   
                        <div class="col-sm-4">
                            <div class="list-of__area">
                                <div class="img__">
                                    <a href="plate.php"><img src="./images/produk-absence.jpg" alt=""></a>  
                                </div>                                
                                <div class="area__text"> 
                                    <div class="header">
                                        <p class="f-cairo"><a href="product.php" class="white"> Smart Absence Management Solutions</a></p>
                                    </div>  
                                    <div class="desc bg-absence">
                                        <p class="f-cairo"><a href="product.php" class="white"> Lorem ipsum dolor sit amet consectetur adipisicing elit. sed officiis pariatur minima minus eius!</a></p>
                                    </div>                          
                                </div>                                                                
                            </div>                                                       
                        </div>                   
                        <div class="col-sm-4">
                            <div class="list-of__area">
                                <div class="img__">
                                    <a href="plate.php"><img src="./images/img-area of interest-1.png" alt=""></a> 
                                </div>                                
                                <div class="area__text"> 
                                    <div class="header">
                                        <p class="f-cairo"><a href="product.php" class="white"> Smart Disaster Prevention Solutions</a></p>
                                    </div>  
                                    <div class="desc bg-disaster2">
                                        <p class="f-cairo"><a href="product.php" class="white"> Lorem ipsum dolor sit amet consectetur adipisicing elit. sed officiis pariatur minima minus eius!</a></p>
                                    </div>                          
                                </div>                                                                
                            </div>                                                       
                        </div>                   
                        <div class="col-sm-4">
                            <div class="list-of__area">
                                <div class="img__">
                                    <a href="plate.php"><img src="./images/img-area of interest-2.png" alt=""></a> 
                                </div>                                
                                <div class="area__text"> 
                                    <div class="header">
                                        <p class="f-cairo"><a href="product.php" class="white"> Smart Parking Solutions</a></p>
                                    </div>  
                                    <div class="desc bg-parking2">
                                        <p class="f-cairo"><a href="product.php" class="white"> Lorem ipsum dolor sit amet consectetur adipisicing elit. sed officiis pariatur minima minus eius!</a></p>
                                    </div>                          
                                </div>                                                                
                            </div>                                                       
                        </div>          
                        <div class="col-sm-4">
                            <div class="list-of__area">
                                <div class="img__">
                                    <a href="plate.php"><img src="./images/img-area of interest-3.png" alt=""></a>
                                </div>                                
                                <div class="area__text"> 
                                    <div class="header">
                                        <p class="f-cairo"><a href="product.php" class="white"> Smart Absence Solution</a></p>
                                    </div>  
                                    <div class="desc bg-absence2">
                                        <p class="f-cairo"><a href="product.php" class="white"> Lorem ipsum dolor sit amet consectetur adipisicing elit. sed officiis pariatur minima minus eius!</a></p>
                                    </div>                          
                                </div>                                                                
                            </div>                                                       
                        </div>                   
                    </div>                        
                </div>
            </div>
        </div>
    </div>
<script src="js/swipper.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
      effect: 'flip',
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      grabCursor: true,
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
<?php include 'footer.php'; ?>