
<div class="top-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="top">We Can't Wait to see what your build <br>Free Options and Free Advice. Everything You need to get started</p>
                <a href="">3 Monts Free</a>
                <a href="contact.php">Contact Us</a>
            </div>
        </div>
    </div>
</div>

<div class="bottom-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-4">                        
                        <div class="bottom-footer__logo">
                            <img src="./images/logo.png" alt="">
                        </div>
                        <div class="bottom-footer__address">
                            <h5>alfabeta office address:</h5>
                            <p class="f-w-200">Jl.Talavera Office Suite lt.18 <br>jl tb.Simatupang kav.22-26<br>Cilandak barat, Jakarta Selatan</p>
                            <br><br>
                            <p>Email: <br> <span class="f-w-200">contact@alfabeta.co.id</span></p>
                            <p>Phone: <br> <span class="f-w-200"></span> 021-71790313 </p>
                            <p class="litle"> 2018 PT.Alfabeta Solusi Nusantara</p>
                        </div>
                    </div>
                    <div class="col-sm-8">                            
                        <div class="bottom-footer__menu">
                            <div class="row">
                                <div class="col-sm-4">Product
                                    <ul>
                                        <li><a href="ocr.php" class="white">OCR (Optical Character Recognition)</a></li>
                                        <li><a href="plate.php" class="white">License Plate Recognition</a></li>
                                        <li><a href="flood.php" class="white">Flood Detection</a></li>
                                        <li><a href="crowd.php" class="white">Crowd Detection</a></li>
                                        <li><a href="fire-detection.php" class="white">Fire Detection</a></li>
                                        <li><a href="face-detection.php" class="white">Face Detection</a></li>
                                        <li><a href="human-detection.php" class="white">Human Detection</a></li>
                                        <li><a href="intruder.php" class="white">Intruder Detection</a></li>
                                        <li><a href="vehicle.php" class="white">Vehicle Detection</a></li>
                                        <li><a href="trash.php" class="white">Trash Detection</a></li>
                                    </ul>
                                </div>
                                <!-- <div class="col-sm-4">Solutions
                                    <ul>
                                        <li><a href="product.php" class="white">Smart Parking</a></li>
                                        <li><a href="smart-absence.php" class="white">Smart Absence Management</a></li>
                                        <li><a href="perimeter.php" class="white">Smart Perimeter Security</a></li>
                                        <li><a href="smart-disaster.php" class="white">Smart Disaster and Anomaly Detection</a></li>
                                        <li><a href="product.php" class="white">Attributes</a></li>
                                    </ul>                                    
                                </div> -->
                                <div class="col-sm-4">About Us
                                    <ul>
                                        <!-- <li><a href="terms.php" class="white">What is Alfabeta?</a></li>
                                        <li><a href="terms.php" class="white">Privacy Policy</a></li>
                                        <li><a href="terms.php" class="white">Terms of Service</a></li> -->
                                        <li><a href="faq.php" class="white">FAQ</a></li>
                                    </ul>
                                </div> 
                            </div>
                        </div>   
                        <div class="form-social">
                        <div class="menu">
                            <ul>
                                <li class="facebook"><a  href=""><i class="fab fa-facebook-f"></i></a></li>
                                <li class="google"><a href=""><i class="fab fa-google-plus-g"></i></a></li>
                                <li class="twitter"><a href=""><i class="fab fa-twitter"></i></a></li>
                                <li class="linkedin"> <a href=""><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>                                                        
                    </div>
                </div>
            </div>          
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </body>
</html>