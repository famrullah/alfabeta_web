<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/services.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/header.css"> 
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>Alfabeta</title>
  </head>
  <body>
        <nav class="navbar navbar-expand-lg nav_alfabet">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img id="logo" src="./images/logo.png" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Beranda <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#one" target="about">Tentang Kami <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#two">Solutions <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#three">Portfolio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php" target="about">Kontak</a>
                        </li>
                    </ul>
                </div>
                <!-- <form class="form-inline my-2 my-lg-0">                        
                    <a class="btn btn-outline-success my-2 my-sm-0 btn-services2" href="index.php"><i class="fas fa-globe"></i> &nbsp;Our Product</a>
                </form> -->
            </div>            
        </nav>
<script>
	$(window).scroll(function(){
		
		if ($(window).scrollTop() >= $(window).height() - 80) {
			$('.nav_alfabet').addClass('navbar-dark');
			// $('.header .logo img').attr('src', "http://tulus.appshouse.co.id/wp-content/themes/tulus/img/logo-white.png");
		}else{
			$('.nav_alfabet').removeClass('navbar-dark');
			// $('.header .logo img').attr('src', "http://tulus.appshouse.co.id/wp-content/themes/tulus/img/logo-black.png");
		}

	})

    $(document).ready(mysize);
	$(window).on('resize',mysize);

	function mysize(){
		$('.banner').css({
            'height': $(window).height(),
        })
        
    }
    
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');
        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });
        return false;
    });

   

</script>
