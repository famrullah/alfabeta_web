<?php include 'navbar.php'; ?>    
    <div class="banner-product banner-trash">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="f-cairo">Trash Detection</h1>
                </div>
                <div class="col-sm-6">
                    <p class="f-cairo f-w-200 f-14">
                        Deteksi sampah pada suatu area, seperti sungai, jalan, dan area publik lainnya 
                    </p>
                </div>
            </div>
        </div>            
    </div>    
    <div class="product-page">
        <div class="product-page__img">
            <img src="./images/trash.jpeg" alt="">
        </div>
        <div class="product-page__desc">
            <h2>Identifikasi menurut ratio</h2>
            <p class="f-cairo">
                Metode perbandingan antara luas daerah pemantauan dengan objek selain air
            </p><br>
            <h2>Data Analitik</h2>
            <p class="f-cairo">
                Data persatuan waktu akan tersimpan dalam database untuk digunakan sebagai alat utama dalam 
                pengambilan keputusan. Seperti :<br>
                1. Tumpukan sampah per periode <br>
                2. Waktu pembersihan sungai <br>
                3. Tingkat pencemaran
            </p>
        </div>
    </div>
    
    <!-- <div class="product__bg">
        <div class="desc">
            <p class="white">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.\
                In omnis vitae consequatur maxime. Assumenda doloremque similique distinctio modi ad nemo corporis vitae animi vel, quis pariatur sunt sequi, numquam quibusdam!
            </p>
        </div>
    </div> -->
    <!-- <div class="product__content">
        <div class="container-fluid">
            <div class="row">                        
                <h1>Smart Parking</h1>
            </div>
        </div> 

        <div class="container">
            <div class="row">
                    <div class="col-sm-5">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita ad dolore, amet nemo rerum ut necessitatibus enim excepturi! Minima est, 
                            perspiciatis dolorum inventore fuga sit qui blanditiis error officiis dolor.</p>
                    </div>
                    <div class="col-sm-7">        
                </div>
            </div>
        </div>           
    </div> -->

    <!-- <div class="product__content-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-5"></div>  
                <div class="col-sm-7">
                    <img src="./images/xtrans.png" alt="">  
                </div>  
            </div>                
        </div>
    </div> -->

    <div class="our-partners">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h2 class="f-cairo">Our Partner</h2>
                    </div>                        
                </div>
                <div class="col-sm-3">
                    <img src="./images/ic-cctv.png" alt="" style="width:50%">
                </div>
                <div class="col-sm-3">
                    <img src="./images/ic-cctv.png" alt="" style="width:50%">
                </div>
                <div class="col-sm-3">
                    <img src="./images/ic-cctv.png" alt="" style="width:50%">   
                </div>
                <div class="col-sm-3">
                    <img src="./images/ic-cctv.png" alt="" style="width:50%">   
                </div>
            </div>
        </div>
    </div>
    
<?php include 'footer.php'; ?>