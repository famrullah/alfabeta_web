<?php include 'navbar.php'; ?>    
    <div class="banner-product banner-human">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="f-cairo">Crowd Detection</h1>
                </div>
                <div class="col-sm-6">
                    <p class="f-cairo f-w-200 f-14">
                    Mendeteksi kerumunan orang yang memiliki potensi terjadinya aktivitas abnormal (ie: demo, tawuran,
                    dan lain sebagainya) pada suatu area tertentu
                    </p>
                </div>
            </div>
        </div>            
    </div>    
    <div class="product-page">
        <div class="product-page__img">
            <img src="./images/human.jpg" alt="">
        </div>
        <div class="product-page__desc">
            <p class="f-cairo">Mendeteksi kerumunan orang yang memiliki potensi terjadinya aktivitas abnormal 
                                (ie : tawuran,dan lain sebagainya) pada suatu area tertentu
            </p>
        </div>
    </div>
    
    <!-- <div class="product__bg">
        <div class="desc">
            <p class="white">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.\
                In omnis vitae consequatur maxime. Assumenda doloremque similique distinctio modi ad nemo corporis vitae animi vel, quis pariatur sunt sequi, numquam quibusdam!
            </p>
        </div>
    </div> -->

    <!-- <div class="product__content">
        <div class="container-fluid">
            <div class="row">                        
                <h1>Smart Parking</h1>
            </div>
        </div> 

        <div class="container">
            <div class="row">
                    <div class="col-sm-5">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita ad dolore, amet nemo rerum ut necessitatibus enim excepturi! Minima est, 
                            perspiciatis dolorum inventore fuga sit qui blanditiis error officiis dolor.</p>
                    </div>
                    <div class="col-sm-7"> 
                </div>
            </div>
        </div>           
    </div> -->

    <!-- <div class="product__content-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-5"></div>  
                <div class="col-sm-7">
                    <img src="./images/xtrans.png" alt="">  
                </div>  
            </div>                
        </div>
    </div> -->

    <div class="our-partners">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h2 class="f-cairo">Our Partner</h2>
                    </div>                        
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">   
                </div>
                <div class="col-sm-3">
                    <img src="./images/dahua.png" alt="" style="width:50%">   
                </div>
            </div>
        </div>
    </div>
    
<?php include 'footer.php'; ?>