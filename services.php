<?php include "header2.php"?>
<div class="services-banner">
    <div class="services-text">
        <div class="title">
            <h1>Lorem Ipsum Is</h1>
        </div>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique quo quos laborum illum quaerat alias animi fugit, at, 
            natus dolorum repellat ea commodi? Laudantium facere sint modi mollitia autem eligendi?</p>
    </div>
    <div class="services-arrow">
        <i class="fas fa-arrow-down"></i>
        <p>Scroll Down</p>
    </div>
</div>
<div class="services-content" id="one">
    <div class="services-content__left">
        <div class="services-content__title">
            <h3>Loerm Ipsum ?</h3>
        </div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit at, 
                autem iste fugiat voluptates fugit officia ex numquam impedit nobis consectetur excepturi debitis corporis dolores tenetur beatae animi, nam dignissimos.
            </p>
    </div>
    <div class="services-content__right">
        <img src="./images/parkingspace.jpg" alt="">
    </div>
</div>  
<div class="services-area">
    <div class="services-area__text">
        <div class="services-area__title">
            <h2>Area of Interest</h2>
        </div>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur aliquam officiis sit libero vero fugit voluptatem nesciunt,
            error qui at dolorum sunt totam iusto corrupti in maxime rerum quos ab?
        </p>
    </div>
    <div class="services-area__image">
        <div class="services-area__image-content">
            <img src="./images/produk-absence.jpg" alt="">
            <img src="./images/produk-security.jpg" alt="">
            <img src="./images/produk-disaster.jpg" alt="">
            <img src="./images/transportation.png" alt="">
            <img src="./images/fire.jpg" alt="">
        </div>
    </div>
</div>

<div class="services-project" id="two">
    <div class="services-project__title">
        <h1>Project We've Working On</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit at</p>
    </div>
    <div class="services-project__content">
        <div class="left">
            <div class="description">
                <h2>Business Analytical Solution</h2>
                <p>Alfabeta provides services to help you analyze your company data, such as:</p>
                <ul>
                    <li>1. Business Analytic Dashboard</li>
                    <li>2. Smart Filtering and Searching</li>
                    <li>3. Chart, Graph, and Reporting</li>
                </ul>
                <div class="portfolio">
                    <a href="">See Portofolio</a>
                </div>    
            </div>                    
        </div>
        <div class="right">
            <img src="./images/businessanalytical.jpg" alt="">
        </div>
    </div>
    <div class="services-project__content">
        <div class="left">
            <img src='../images/interest.png'alt="">  
        </div>
        <div class="right">
            <div class="description">
                <h2>Business Process Optimization Solutions</h2>
                <p>Alfabeta provides services to help you optimize your company's tools, such as:</p>
                <ul>
                    <li>1. Database Optimization</li>
                    <li>2. Consulting Load Balancing / Scaling Server</li>
                    <li>3. Software Optimization</li>
                </ul>
                <div class="portfolio">
                    <a href="">See Portofolio</a>
                </div>    
            </div>  
        </div>
    </div>
    <div class="services-project__content">
        <div class="left">
            <div class="description">
                <h2>E-Government Solutions</h2>
                <p>Alfabeta provides services to assist governments with end-to-end solution services:</p>
                <ul>
                    <li>1. E-Filing</li>
                    <li>2. E-Budgeting (RKPJMD, RKPD, PPAS / PPAD, RAB)</li>
                    <li>3. E-Catalog / E-Procurement</li>
                    <li>4. E-MonEv (Monitoring and Evaluation)</li>
                    <li>5. E-Absence</li>
                    <li>6. GIS (Geographical Information System)</li>                                                                                   
                </ul>
                <div class="portfolio">
                    <a href="">See Portofolio</a>
                </div>    
            </div>                    
        </div>
        <div class="right">
            <img src="./images/intruder.jpg" alt="">
        </div>
    </div>
</div>

<div class="services-portfolio" id="three">
    <div class="services-portfolio__title">
        <h1>Portfolio</h1>        
    </div>
    <div class="menu">
        <p>All</p>
        <p>Data Analisis</p>
        <p>Big Data</p>
        <p>Analisis Data</p>
        <p>Data Big</p>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/Portfolio1.png" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/PORTFOLIO5.png" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/produk-parking.jpg" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/businessanalytical.jpg" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/services-egovernment.jpg" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/xtrans.png" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/offices.png" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="portfolio-content">
        <div class="image">
            <img src="./images/flood.jpg" alt="">
        </div>
        <div class="desc">
            <p class="title">Lorem</p>
            <p class="text">lorem ipsum dolor sit amet consectetur adipisicing</p>
        </div>
    </div>
    <div class="load-more">
        <a href="">Load More</a>
    </div>
</div>

<div class="services-contact" id="four">
    <div class="form"></div>
    <div class="address"></div>
</div>

<script>

// $(document).ready(function(){
//     $('.nav-link').click(function(event){
//        event.preventDefault();
//        window.scrollTo(2000, 2000);
//     })
// })
</script>
<?php include "footer.php"?>
