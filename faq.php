<?php include "navbar.php"?>
<div class="banner-product banner-faq f-cairo">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="f-cairo">FAQ</h1>
            </div>
            <div class="col-sm-6">
                <p class="f-cairo f-w-200 f-14">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Et cum ad alias sed aliquam, repudiandae neque facere nihil dolorem perferendis. 
                    Consectetur odio quas eveniet doloribus, hic magnam animi voluptates 
                </p>
            </div>
        </div>
    </div>            
</div>  
<div class="container f-cairo">
    <div class="row">
        <div class="sidebar">
            <ul>
                <li id="one">lorem ipsum simply?</li>
                <li id="two">lorem ipsum simply?</li>
                <li id="three">lorem ipsum simply?</li>
                <li id="four">lorem ipsum simply?</li>
                <li id="five">lorem ipsum simply?</li>
            </ul>
        </div>
        <div class="faq">
            <div id="faq-content" class="one">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil fugit libero, dolorem quisquam error recusandae eveniet? Sunt similique quibusdam consequuntur nostrum nihil, repudiandae sed fugiat alias quia, modi aliquam quos!</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, iure magni quae nam inventore, eveniet, architecto accusamus nulla omnis cupiditate aperiam suscipit. Laudantium quia nisi nulla dolor, fugiat molestias odit.</p>
            </div>
            <div id="faq-content" class="two">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil fugit libero, dolorem quisquam error recusandae eveniet? Sunt similique quibusdam consequuntur nostrum nihil, repudiandae sed fugiat alias quia, modi aliquam quos!</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, iure magni quae nam inventore, eveniet, architecto accusamus nulla omnis cupiditate aperiam suscipit. Laudantium quia nisi nulla dolor, fugiat molestias odit.</p>
            </div>
            <div id="faq-content" class="three">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil fugit libero, dolorem quisquam error recusandae eveniet? Sunt similique quibusdam consequuntur nostrum nihil, repudiandae sed fugiat alias quia, modi aliquam quos!</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, iure magni quae nam inventore, eveniet, architecto accusamus nulla omnis cupiditate aperiam suscipit. Laudantium quia nisi nulla dolor, fugiat molestias odit.</p>
            </div>
            <div id="faq-content" class="four">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil fugit libero, dolorem quisquam error recusandae eveniet? Sunt similique quibusdam consequuntur nostrum nihil, repudiandae sed fugiat alias quia, modi aliquam quos!</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, iure magni quae nam inventore, eveniet, architecto accusamus nulla omnis cupiditate aperiam suscipit. Laudantium quia nisi nulla dolor, fugiat molestias odit.</p>
            </div>
            <div id="faq-content" class="five">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil fugit libero, dolorem quisquam error recusandae eveniet? Sunt similique quibusdam consequuntur nostrum nihil, repudiandae sed fugiat alias quia, modi aliquam quos!</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, iure magni quae nam inventore, eveniet, architecto accusamus nulla omnis cupiditate aperiam suscipit. Laudantium quia nisi nulla dolor, fugiat molestias odit.</p>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.one').on('click',function(){
           console.log()
        })
        
    })
</script>
<?php include "footer.php"?>