<?php include 'navbar.php'; ?>
    
    <div class="banner-product">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="f-cairo">Tentang Kami</h1>
                </div>
                <div class="col-sm-6 ">
                    <p class="f-cairo f-w-200 f-14">
                        Alfabeta adalah penyedia solusi berupa produk dan layanan end-to-end di berbagai industri seperti transportasi, perbankan, perumahan, logistik, dan lain-lain. 
                        Kami menitik-beratkan layanan kami untuk meningkatkan efisiensi kinerja bisnis Anda.
                    </p>
                </div>
            </div>
        </div>            
    </div>

    <div class="about-us__content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 about-left">
                    <img src="http://alfabeta.co.id/wp-content/themes/alfabeta/img/asset/banner-2.jpg" alt="">
                </div>
                <div class="col-sm-6 about-right">
                    <h4 class="about-us__content-title f-cairo"></h4>
                    <p class="f-cairo"> Semua produk dan layanan yang kami tawarkan berbasis teknologi paling
                                        mutakhir (up to date), yang menggunakan algoritma-algoritma pintar seperti
                                        <i>Object Detection, Deep Learning, Big Data,</i> dan <i>Analytics</i>.
                                        Tim R & D kami berdedikasi tinggi agar terus bisa melahirkan inovasi baru 
                                        yang berkualitas tinggi
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us__bg"> </div>

    <div class="about-us__content">
        <div class="container-fluid">
            <div class="row">                        
                <div class="col-sm-6 about-right">
                    <h4 class="about-us__content-title f-cairo"></h4>
                    <p>Semua produk dan layanan yang kami tawarkan berbasis teknologi paling
                        mutakhir (up to date), yang menggunakan algoritma-algoritma pintar seperti
                        <i>Object Detection, Deep Learning, Big Data,</i> dan <i>Analytics</i>.
                        Tim R & D kami berdedikasi tinggi agar terus bisa melahirkan inovasi baru 
                        yang berkualitas tinggi
                    </p>
                </div>
                <div class="col-sm-6 about-left ">
                        <img src="http://alfabeta.co.id/wp-content/themes/alfabeta/img/asset/banner-2.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>